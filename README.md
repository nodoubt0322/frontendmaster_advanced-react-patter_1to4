## advanced react patterns - 01 to 04

#### `course resource`

- repository
  > git clone https://github.com/kentcdodds/advanced-react-patterns-v2.git
- run app
  > npm run start
- run test
  > npm run test
- execise
  > /src/exercise folder
- solution
  > /src/exercise-final folder
- codesandbox
  > https://codesandbox.io/s/github/kentcdodds/advanced-react-patterns-v2

---

#### `downshift` (build by paypal)

[repository](https://github.com/paypal/downshift)

> Primitive to build simple, flexible, WAI-ARIA compliant enhanced `input React components`  
> WAI-ARIA 簡單理解為`無障礙網頁`

[demo](https://codesandbox.io/s/6z67jvklw3)

---

#### `exercise code`

01.js

```js
import React from 'react'
import {Switch} from '../switch'

class Toggle extends React.Component {
  //http://egorsmirnov.me/2015/08/16/react-and-es6-part3.html
  state = {on: false}
  //use es6 ClassProperty to bind this
  toggle = () =>
    this.setState(
      ({on}) => ({on: !on}),
      () => {
        this.props.onToggle(this.state.on)
      },
    )
  //https://reactjs.org/docs/react-component.html#setstate
  //setState(updater[, callback])
  //setState是async, 會在setState確定執行完之後,執行callback

  render() {
    const {on} = this.state
    return <Switch on={on} onClick={this.toggle} />
  }
}

function Usage({
  //dumb component, return jsx

  //onToggle是參數, 預設值是一個function
  onToggle = (...args) => console.log('onToggle', ...args),
  //把丟進來的參數spread, 這案例只有this.state.on一個參數
}) {
  return <Toggle onToggle={onToggle} />
}
Usage.title = 'Build Toggle'

export {Toggle, Usage as default}
//相當於 export Toggle
//      export default Usage
```

---

#### `Using a function in setState instead of an object ?`

[3 Reasons why I stopped using React.setState](https://blog.cloudboost.io/3-reasons-why-i-stopped-using-react-setstate-ab73fc67a42e)

react build-in (async)

![alt build-in](https://cdn-images-1.medium.com/max/1600/1*v2qbGqdV8wM1G4ixs7woEw.gif)

mobx (sync)

![alt mobx](https://cdn-images-1.medium.com/max/1600/1*LPl8MGfkPyWGtRERQdw_3w.gif)

setState 方法與包含在其中的執行是一個很複雜的過程，這段程式碼從 React 最初的版本到現在，也有無數次的修改。  
它的工作除了要更動 this.state 之外，還要負責觸發重新渲染(render)，這裡面要經過 React 核心中`diff演算法`，  
最終才能決定`是否要進行重渲染`，以及`如何渲染`。而且為了`批次與效能`的理由，  
`多個setState`呼叫有可能在執行過程中還需要被`合併`，所以它被設計以異步的或延時的來進行執行是相當合理的。  

因為 state 是 async, 所以透過 function call 去確定帶進去的 state 是如我們所預期
```js
this.setState(currentState => {on: !currentState.on})
```
